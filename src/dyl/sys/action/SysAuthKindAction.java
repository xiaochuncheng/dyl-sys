package dyl.sys.action;

import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.Page;
import dyl.sys.bean.SysAuthKind;
import dyl.sys.service.SysAuthKindServiceImpl;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * 2017-04-14 17:37:34
 */
@Controller
public class SysAuthKindAction extends BaseAction{
	@Resource
	private JdbcTemplate jdbcTemplate;
	@Resource
	private SysAuthKindServiceImpl sysAuthKindServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@RequestMapping(value = "/sysAuthKind!main.do")
	public String main(Page page,SysAuthKind sysAuthKind,HttpServletRequest request){
		try{
			request.setAttribute("sysAuthKindList",sysAuthKindServiceImpl.findSysAuthKindList(page, sysAuthKind));
			request.setAttribute("sysAuthKind", sysAuthKind);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return "/sys/sysAuthKind/sysAuthKindMain";
	}
	 /**
	 * 说明：添加或修改
	 * @return String
	 */
	@RequestMapping(value = "/sysAuthKind!sysAuthKindForm.do")
	public String  sysAuthKindForm(BigDecimal id,HttpServletRequest request){
		try {
			if(id!=null)request.setAttribute("sysAuthKind",sysAuthKindServiceImpl.getSysAuthKind(id));
		} catch (Exception e){
			throw new CommonException(request,e);
		}
		return "/sys/sysAuthKind/sysAuthKindForm";
	}
	/**
	 * 说明：更新或者插入sys_auth_kind
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/sysAuthKind!add.do")
	public R add(SysAuthKind sysAuthKind,HttpServletRequest request){
		try {
			int ret = sysAuthKindServiceImpl.insertSysAuthKind(sysAuthKind);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：更新或者插入sys_auth_kind
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/sysAuthKind!update.do")
	public R update(SysAuthKind sysAuthKind,HttpServletRequest request){
		try {
			int	ret = sysAuthKindServiceImpl.updateSysAuthKind(sysAuthKind);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：根据主键删除表sys_auth_kind中的记录
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/sysAuthKind!delete.do")
	public R deleteSysAuthKind(String  dataIds,HttpServletRequest request){
		try {
			int ret = sysAuthKindServiceImpl.deleteSysAuthKind(dataIds);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
}
