/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-04-14 15:31:41
*/
package dyl.sys.bean;

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;
import java.math.BigDecimal;
import java.util.Date;

public class SysRoleUser implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：主键
	*对应db字段名:id 类型:NUMBER(22) 主键 
	*是否可以为空:否
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：角色id
	*对应db字段名:roleid 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  roleid;
	
	/*字段说明：用户id
	*对应db字段名:userid 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  userid;
	
	/*字段说明：创建时间
	*对应db字段名:create_time 类型:DATE(7)  
	*是否可以为空:是
	*/
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private  Date  createTime;
	
	/*字段说明：创建人
	*对应db字段名:creator 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  creator;
	
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public BigDecimal getRoleid() {
        return roleid;
    }
    public void setRoleid(BigDecimal roleid) {
        this.roleid = roleid;
    }
    public BigDecimal getUserid() {
        return userid;
    }
    public void setUserid(BigDecimal userid) {
        this.userid = userid;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public BigDecimal getCreator() {
        return creator;
    }
    public void setCreator(BigDecimal creator) {
        this.creator = creator;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"roleid:"+roleid+"\n"+
		
		"userid:"+userid+"\n"+
		
		"createTime:"+createTime+"\n"+
		
		"creator:"+creator+"\n"+
		"";
	}
}
